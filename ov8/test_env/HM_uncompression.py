import string

with open('./HM_compressed_text', 'rb') as file:
    text = file.read()


extended_string = '¡¢£¤¥¦§¨©ª«¬­®¯°±²³´µ¶·¸¹º»¼½¾¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿ'


i = 0
meta_data = ''
while True:
    if meta_data[-9:] == 'END TABLE':
        break
    try: 
        meta_data += text[i:i+1].decode('UTF-8')
        i += 1
    except:
        try:
            meta_data += text[i:i+2].decode('UTF-8')
            i += 2
        except:
            meta_data += text[i:i+3].decode('UTF-8')
            i += 3
    
text = text[i+2:]
meta_data = meta_data.split('\n')

rep_tree_arr = []
class rep_tree():
    def __init__(self, symb, val):
        self.symb = symb
        self.val = val

newline_char = False
for i in meta_data:
    if i == meta_data[-1]:
        i = i.split()
        byte_count = int(i[0])
        rest1 = int(i[1])
    else:    
        if len(i) > 0:
            char = i[0]
            value = i[1:]
        else: 
            newline_char = True 
            continue
        if newline_char:
            char = '\n'
            value = i
            newline_char = False 
        rep_tree_arr.append(rep_tree(char, value))



# for k in rep_tree_arr:
#     print(k.symb, k.val)


huffman_code = text[:byte_count]
lz_code = text[byte_count:]

bit_string = ''
for i in range(len(huffman_code)):
    num = int.from_bytes(huffman_code[i:i+1],'little')
    bi = bin(num).lstrip('0b')
    if len(bi) != 8:
        rest = 8 - len(bi)
        bi = '0' * rest + bi
    bit_string += bi
    
bit_string = bit_string[:-rest1]

huff_string = ""
tmp_value = ''
for k in range(len(bit_string)):
    tmp_value += bit_string[k]
    a = [a for a in rep_tree_arr if a.val == tmp_value]
    if len(a) > 0: 
        huff_string += a[0].symb
        tmp_value = ''

full_code = b''
next_chars = 0
with open('./HM_uncompressed_text', 'w') as file:
        file.write('')
for i in range(0,len(lz_code),6):
    counter = 0
    value = lz_code[i:i+2]
    value = int.from_bytes(value, 'little')
    with open('./HM_uncompressed_text', 'ab') as file:
        file.write(value.to_bytes(2, 'little'))
    j  = 0
    a = len(huff_string[:value])
    while True: 
        if j >= a:
            break
        if huff_string[:value][j] not in string.printable and huff_string[:value][j] not in extended_string:
            a -= 2
            counter += 2
        elif huff_string[:value][j] not in string.printable:
            a -=1
            counter += 1
        j += 1
    with open('./HM_uncompressed_text', 'a') as file:
        file.write(huff_string[:value-counter])
    huff_string = huff_string[value-counter:]
    if i + 4 < len(lz_code):
        with open('./HM_uncompressed_text', 'ab') as file:
            file.write(lz_code[i+2:i+4])
            file.write(lz_code[i+4:i+6])
    


