from io import TextIOBase
from math import floor, sqrt
from os import error
import sympy
import random
import timeit

#count lenght
lenght = 100000
#get next prime
table_lenght = sympy.nextprime(lenght)
#define crashcounter
crashcounter = 0
#define array
arr = []
#fill array with numbers 
for i in range(table_lenght):
    arr.append(i+1)
#schuffle array
random.shuffle(arr)
#define A
a = (sqrt(5)-1)/2

print("setup finished")


class HashTable(object):
    global table_lenght
    global a
    #init table with lenght based off the next prime after the lenght variable
    def __init__(self):
        self.array = [None] * table_lenght

    #hashfunction 
    def hash(self, key):
        index = floor(table_lenght * ((a * key)-floor(a * key)))
        return index

    #get the whole table
    def getAll(self):
        return self.array

    #add name using the hashfunction
    def add(self, key):
        global crashcounter
        index = self.hash(key)
        if self.array[index] is None:
            self.array[index] = key
        else:
            for k in range(table_lenght-1):
                crashcounter += 1 
                if index+k >= table_lenght-1:
                    if self.array[index-k] is None:
                        self.array[index] = key
                        break
                elif self.array[index+k] is None:
                    self.array[index+k] = key
                    break
            else:
                raise IndexError('No empty indexes, table full') 

    #get element using the hashfunction
    def get_element(self, key):
        index = self.hash(key)
        if self.array[index] is None:
            raise KeyError('No element on keyindex')
        else:
            if self.array[index] == key:
                return self.array[index]
            else:
                for k in range(table_lenght-1):
                    #check if we cant search upwards
                    if index+k > table_lenght-1:
                        #print("searching downwards")
                        if self.array[index+k-(table_lenght-1)] is not None and self.array[index] == key:
                            return self.array[index+k-(table_lenght-1)]
                        elif self.array[index-k] is None:
                            raise LookupError('Couldnt find value downwards')                     
                    else:
                        #searching upwards
                        #print("searching upwards")
                        if self.array[index+k] is not None and self.array[index] == key:
                            return self.array[index]
                        elif self.array[index+k] is None:
                            raise LookupError('Couldnt find value upwards')

                else:
                    IndexError('Couldnt find index in database') 






ht = HashTable()


#fill table with values from number array
#% of names to fill the table with
#exmaple c = 0.5 equals 50% of numbers 
c = 0.5
b = floor(table_lenght * c)
print("adding {} percent of numbers ({}) to hashtable".format(int(c*100), b))
start = timeit.default_timer()
for i in arr[:b]:
    ht.add(i)
stop = timeit.default_timer()
time = stop - start
#ht.add(10)
print("numbers added")
#print("Getting number 10: ", ht.get_element(10))
print("crashcounter: ", crashcounter)
print("time: ", time )
