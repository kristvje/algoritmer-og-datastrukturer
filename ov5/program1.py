import sympy
import timeit

#insert path to namefile below
with open('./navn.txt') as file:
    name_file = file.read()

#count number of names in file
lenght = name_file.count("\n")
#get next prime
table_lenght = sympy.nextprime(lenght)
name_file_split = name_file.splitlines()
#define crashcounter
crashcounter = 0



class HashTable(object):
    global table_lenght
    #init table with lenght based off the next prime after the number of names in namefile
    def __init__(self):
        self.array = [None] * table_lenght

    #hashfunction 
    def hash(self, key):
        value = 0
        cur_pot = 1
        for i in range(0, len(key)-1):
            uni = ord(key[i])
            value +=  uni * cur_pot 
            cur_pot *= 7
        return value % table_lenght

    #get the whole table
    def getAll(self):
        return self.array

    #add name using the hashfunction
    def add(self, key):
        index = self.hash(key)
        if self.array[index] is None:
            self.array[index] = []
            self.array[index].append(key)
        else:
            #print crashes on index, as well as add 1 to the total counter 
            global crashcounter
            crashcounter += 1 
            #print("crash on index: ", index)
            self.array[index].append(key)

    #get element using the hashfunction
    def get_element(self, key):
        index = self.hash(key)
        if self.array[index] is None:
            raise KeyError('No element on keyindex')
        else:
            if len(self.array[index]) == 1 and self.array[index][0] == key:
                return self.array[index][0]
            else:
                for element in self.array[index]:
                    if element == key:
                        return element
                else:
                    raise ValueError('Couldnt find value on keyindex')







ht = HashTable()
#fill table with values from namefile
start = timeit.default_timer()
for name in name_file_split:
    ht.add(name)
stop = timeit.default_timer()
time = stop - start 
print("totale kræsj: ", crashcounter, " og lastfaktor: ", lenght/table_lenght)
print("kræsj per pers: ", crashcounter/lenght)
print("henter Jacob Theisen: ", ht.get_element("Jacob Theisen"))
print("Time: ", time)
#print(ht.getAll())

