import sys
import timeit

sys.setrecursionlimit(16000)
arr = []

def rec(x, n):
    if n < 0: 
        return "n is negative"  
    if n == 0:
        return 1
    if n == 1: 
        return x
    if n%2==0:
        #print("partall:", x, n)
        return rec(x*x, n/2)
        #rec(x, n/2) * rec(x, n/2)
        #(x*x)**(n/2)
    else:   
        #print("oddetall: ", x, n)
        return x * rec(x*x, (n-1)/2)


#print(rec(float(sys.argv[1]), int(sys.argv[2])))


def oppg2(x, n):
    start = timeit.default_timer()
    #print(rec(x,n))
    rec(x,n)
    stop = timeit.default_timer()
    #print("timer: ", stop-start)
    time = stop - start
    arr.append(time) 

for i in range(0, 10000):
    oppg2(float(sys.argv[1]), int(sys.argv[2]))
