#prep & read file 
with open("./fil.txt") as file:
    node_file_unsplit = file.read()

node_file = node_file_unsplit.split()

num_node = int(node_file[0])
num_kant = int(node_file[1])
node_file.pop(0) and node_file.pop(0)
#prep done

class Con(object):
    def __init__(self, from_node, to_node, cap, flyt):
        self.from_node = from_node
        self.to_node = to_node
        self.cap = cap 
        self.flyt = flyt


class Nodes(object):
    def __init__(self, node, pre, dist):
        self.node = node
        self.pre = pre
        self.dist = dist

class NodeTable(object):
    def __init__(self):
        self.all_cons = []
        self.array_finished_nodes = []
        self.arr_travel = []
        self.start = ""


    def add_all(self, from_node, to_node, cap, flyt):
                self.all_cons.append(Con(int(from_node), int(to_node), int(cap), int(flyt)))
    
    def check_all_routes(self):
        #check if route a to b has a opposite route b to a, if not add b to a
        for elem in self.all_cons:
            for elem2 in self.all_cons:
                if elem.from_node == elem2.to_node and elem.to_node == elem2.from_node:
                    break
            else:
                self.all_cons.append(Con(elem.to_node, elem.from_node, elem.cap, elem.cap))

    def calculate(self, start, end):
        self.array_finished_nodes = []
        self.arr_travel = []
        queue = []
        self.start = start
        queue.append(Nodes(start, " ", 0))
        #min_dist schould always start of as highet then any potenial capasatiy 
        min_dist = 100000000000000000000
        
        #while queue is not empty do:
        while queue:
            current_node = queue.pop(0)
            #if were on endnode, use rec_func to determine the path through the finishednodes array 
            if current_node.node == end:
                self.rec_func(current_node)
                #find smallest differnce between flyt and cap
                for index in range(len(self.arr_travel)-1, 0, -1):
                    a = next(n for n in self.all_cons if n.from_node==self.arr_travel[index] and n.to_node == self.arr_travel[index-1])
                    diff = int(a.cap - a.flyt)
                    if diff < min_dist:
                        min_dist = diff
                #update all flyt thats find in rec_func through the path from startnode to endnode  
                for index2 in range(len(self.arr_travel)-1, 0, -1):
                    a = next(i for i,n in enumerate(self.all_cons) if n.from_node==self.arr_travel[index2] and n.to_node == self.arr_travel[index2-1])
                    self.all_cons[a].flyt += min_dist
                    #update oposite connections with the opposite value 
                    for elem in self.all_cons:
                        if self.all_cons[a].from_node == elem.to_node and self.all_cons[a].to_node == elem.from_node:
                            elem.flyt -= min_dist
                            break
            #if were not on endnode keep doing bfs to find new nodes            
            else: 
                for i in self.all_cons:
                    node_is_done = 0
                    node_in_queue = 0
                    #find new node that has a positive differnce between cap and flyt
                    if current_node.node == i.from_node and i.cap - i.flyt > 0:
                        #check if we've already finsihed this node 
                        for j in self.array_finished_nodes:
                            if i.to_node == j.node:
                                node_is_done = 1
                        #check of we've already queued this node
                        for k in queue:
                            if i.to_node == k.node:
                                node_in_queue = 1
                                #if we've queued this node, check if we can update distance and pre-node
                                if current_node.dist+1 < j.dist:
                                    j.dist = current_node.dist+1 
                                    j.pre = current_node.node
                        #we've not queued or finished this node
                        if node_is_done == 0 and node_in_queue == 0:
                            #add node to queue
                            queue.append(Nodes(i.to_node, current_node.node, current_node.dist + 1))
            #add current node to finished nodes 
            self.array_finished_nodes.append(current_node) 
        #sort array of x.node value 
        self.array_finished_nodes.sort(key=lambda x: int(x.node))  
       
        #reverse arr_traver array 
        rev_arr_tra = list(reversed(self.arr_travel))
        #if rev_arr_tra is not null, it means weve found a path, return path and min_dist(økning)
        if rev_arr_tra:
            return rev_arr_tra, min_dist
        #if rev_arr_tra is null, weve not found a path, return 1
        else:
            return 1

    def print(self):
        print("all cons: ")
        for i in self.all_cons:
            print(i.from_node, i.to_node, i.cap)
    
    def rec_func(self, node):
        #add node to arr_travel array 
        self.arr_travel.append(node.node)
        #if node.node == self.start weve found the entire path from a to b through the array_finished array
        if node.node == self.start:
            return
        #else call function recurvesly on its prevoius node 
        else:
            a = next(n for n in self.array_finished_nodes if n.node==node.pre)
            nt.rec_func(a)
    
        


#init things
nt = NodeTable()
for e in range(0,len(node_file),3):
    nt.add_all(node_file[e], node_file[e+1], node_file[e+2], 0)
nt.check_all_routes()

counter = 0

start_node = int(input("Startnode: "))
end_node = int(input("Endnode: "))

print("Økning:  vei:")
def run_program():
    global counter
    a = nt.calculate(start_node, end_node)
    #as long as function(a) not returns 1, we find new paths, 
    #print paths and increase total counter for "økning",
    #call itself recursevly
    if a != 1:
        counter +=a[1] 
        print(a[1], a[0])
        run_program()
    #if a is 1 weve not found path, print total couter for 'økning' and exit
    else:
        print("Økning: {}".format(counter))
        return()

run_program()


