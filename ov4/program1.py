list1 = ["(", "[", "{"]
list2 = [")", "]", "}"]

#vikitg at start og slutt for samme type tegn har samme index i hvert sitt array
#legg inn navnet fra samme mappe som skal sjekkes under i open('./example_file')

with open('./test.py') as file:
    codeToTest = file.read()

print(codeToTest)


def check_code(str):
	arr = []
	for i in str:
		if i in list1:
			arr.append(i)
		elif i in list2:
			index = list2.index(i)
			if arr and list1[index] == arr[-1]:
				arr.pop()
			else:
				return "Unbalanced, closing bracket doesent match starting bracket: " + arr[-1] + list2[index]
	if arr:
		return "Unbalanced, extra brackets: " + ''.join(arr)
	else:
		return "Balanced"

print(check_code(codeToTest))


